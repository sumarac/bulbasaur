# Bulbasaur

Bubasaur is my fork of [Tilde](https://github.com/xvvvyz/tilde)

## Basic Usage

To go to a site, type the corresponding key and press return. e.g:

- `g` will redirect you to [gitlab.com](https://gitlab.com)

To search a site, type a space after the site&rsquo;s key followed by your
query. e.g:

- `y kittens` will
  [search YouTube for kittens](https://www.youtube.com/results?search_query=kittens)

To go to a specific path on a site, type the path after the site&rsquo;s key.
e.g:

- `r/r/startpages` will redirect you to
  [reddit.com/r/startpages](https://www.reddit.com/r/startpages)

To access any other site, enter the URL directly. e.g:

- `example.com` will redirect you to [example.com](https://example.com)

## Beyond

Tilde is meant to be customized&mdash;[make it yours!](index.html)

## License

Use and modify Tilde [as you see fit](UNLICENSE).
